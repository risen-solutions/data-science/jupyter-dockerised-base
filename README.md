# Jupyter Dockerised Base



## Getting started

This repo contains a basic setup for dockerised jupyter notebooks. If you know you need particular libraries then these can be added to the requirements.txt file and they will be installed as part of the build step; otherwise libraries can be installed via notebooks.

To run:

docker-compose up --build
